
<?php @include "header.php"; ?>

<div class="inside-banner">
  <img src="assets/images/inside-banner.jpg" alt="Inside Banner">
  <div class="banner-content">
    <h2 class="banner-title">RF Resurgence Downloads</h2>
  </div>
  <div class="shadow"></div>
</div>

<main role="main" class="inside-pages main-wrapper downloads-page">

  <div class="downloads-container">

    <div class="box-border downloads-wrapper">
      <div class="box-border-top"></div>
      <div class="box-border-bottom"></div>
      <div class="downloads-inner-wrapper">
        <table class="downloads-tbl">
          <tr>
            <th>Downloads</th>
            <th>Version</th>
            <th>Patch Size</th>
            <th></th>
          </tr>
          <tr>
            <td>Download Windows 10 Full Client</td>
            <td>Current Version v1.0.0</td>
            <td>1.7 GB</td>
            <td><button type="button" name="btn">Download <span class="chevron"></span></button></td>
          </tr>
          <tr>
            <td>Download Windows 7 Full Client</td>
            <td>Current Version v1.0.0</td>
            <td>2.71 MB</td>
            <td><button type="button" name="btn">Download <span class="chevron"></span></button></td>
          </tr>
          <tr>
            <th colspan="=4">Additional Downloads</th>
          </tr>
          <tr>
            <td>Cash Shop Fix | July 15, 2020</td>
            <td>Current Version v1.0.6</td>
            <td>11.9 KB</td>
            <td><button type="button" name="btn">Download <span class="chevron"></span></button></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="patch-instructions">
      <h4 class="title">Patch Instructions</h4>
      <div class="patch-container">
        
        <div class="patch-wrapper">
          <div class="img-wrapper">
            <img src="assets/images/download-patch.png" alt="">
          </div>
          <p>Download Patch</p>
        </div>

        <img src="assets/images/arrow.png" alt="" class="arrow">
        
        <div class="patch-wrapper">
          <div class="img-wrapper">
            <img src="assets/images/extract.png" alt="">
          </div>
          <p>Extract zip file in your <br/>installation directory.</p>
        </div>

      </div>
    </div>

    <div class="min-requirements">
      <h4 class="title">RF Resurgence Client runs with the minimum recommended specs below.</h4>

      <div class="box-border">
        <div class="box-border-top"></div>
        <div class="box-border-bottom"></div>
        <table class="requirements-table">
          <tr>
            <td>CPU:</td>
            <td>Pentium 4 or Athlon XP or better</td>
          </tr>
          <tr>
            <td>CPU SPEED:</td>
            <td>1.5 GHz</td>
          </tr>
          <tr>
            <td>RAM:</td>
            <td>512 MB+</td>
          </tr>
          <tr>
            <td>OS:</td>
            <td>Windows 7/8/8.1/10</td>
          </tr>
          <tr>
            <td>VIDEO CARD:</td>
            <td>GeForce FX 5200 or Radeon 9600 SE or higher</td>
          </tr>
          <tr>
            <td>DIRECTX VERSION:</td>
            <td>9.0c+</td>
          </tr>
          <tr>
            <td>SOUND CARD:</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>FREE DISK SPACE:</td>
            <td>7.0 GB</td>
          </tr>
          <tr>
            <td>INTERNET:</td>
            <td>10 Mbps+</td>
          </tr>

        </table>
      </div>
        <p class="note">NOTE: IT IS RECOMMENDED TO INSTALL YOUR CLIENT TO YOUR DESKTOP or Drive D</p>
        <p class="note">NOTE: IF YOU HAVE SET YOUR INSTALLATION LOCATION TO PROGRAM FILES (X86) THIS INSTALLATION LOCATION IS PRONE TO PERMISSION ERRORS. PLEASE INSTALL TO PROGRAM FILES ON YOUR OWN DISCRETION.</p>

    </div>

    <div class="additional-requirements">
      <h4 class="title">Additional Requirements</h4>
      <p class="note">NOTE: IF YOU ARE CURRENTLY A WINDOWS 7 USER, PLEASE DOWNLOAD .NET FRAMEWORK BASED ON YOUR CURRENT WINDOWS SYSTEM.</p>
      <p>Please see details below:</p>
      <div class="additional-requirements-container">
        
        <div class="additional-requirements-wrapper">
          <p class="os">Running on Windows 7 SP1 / 8 / 8.1 / 10 :</p>
          <p>Latest .NET Framework</p>
          <button type="button" name="btn">Download <span class="chevron"></span></button>
        </div>
        
        <div class="additional-requirements-wrapper">
          <p class="os">Running on Windows XP & 7:</p>
          <p>.NET Framework 3.5</p>
          <button type="button" name="btn">Download <span class="chevron"></span></button>
        </div>

      </div>
    </div>

    <div class="faqs">
      <h4 class="title">Frequently Asked Questions (FAQ)</h4>
      
      <div class="box-border">
        <div class="box-border-top"></div>
        <div class="box-border-bottom"></div>
        <div class="faqs-wrapper">
          <h5 class="title">A. After installing .NET Framework but still nothing shows up</h5>
          <p>Try to download Windows 7 Launcher and place it inside your RF Client. Run as administrator.</p>
          <p>To make this permanent:</p>
          <ol>
            <li>Right-click the program and go to Properties –> Shortcut.</li>
            <li>Go to Advanced.</li>
            <li>Check Run as Administrator checkbox</li>
          </ol>
          <a href="#" class="reference">Click here for reference</a>
        </div>
        <div class="faqs-wrapper">
          <h5 class="title">B. How to Turn User Account Control On or Off in Windows 10</h5>
          <p>Here's how to turn User Account Control (UAC) on or off in Windows 10:</p>
          <ol>
            <li>Type UAC in the search field on your taskbar. (If the search field isn't visible, right-click the Start button and choose Search.)</li>
            <li>Click Change User Account Control settings in the search results.</li>
            <li>
              Then do one of the following:
              <ul>
                <li>To turn UAC off, drag the slider down to Never notify and click OK.</li>
                <li>To turn UAC on, drag the slider up to the desired level of security and click OK.</li>
              </ul>
            </li>
            <li>You may be prompted to confirm your selection or enter an administrator password.</li>
            <li>Reboot your computer for the change to take effect.</li>
          </ol>
        </div>
      </div>

    </div>



  </div>

<?php @include "footer.php"; ?>