
<?php @include "header.php"; ?>

<main role="main" class="main-wrapper">

  <section class="jobs-section">
    
    <div class="section-title">
      <h2 class="title">Welcome to RF Resurgence</h2>
      <p class="subtitle">Choose your side wisely</p>
    </div>

    <div class="section-content">
      <div class="row">

        <a href="#bellatoInfo" class="job-container col wow fadeInUp bellato-wrapper" data-wow-delay="0.3s">
          <div class="job-wrapper">
            <img src="assets/images/jobs/char_bel_m_46.png" class="job-img" alt="">
            <p class="job-name" data-name="Bellato">Bellato</p>
          </div>
        </a>

        <a href="#" class="job-container col wow fadeInUp cora-wrapper" data-wow-delay="0.3s">
          <div class="job-wrapper">
            <img src="assets/images/jobs/char_cor_m_45.png" class="job-img" alt="">
            <p class="job-name" data-name="Cora">Cora</p>
          </div>
        </a>

        <a href="#" class="job-container col wow fadeInUp accretia-wrapper" data-wow-delay="0.3s">
          <div class="job-wrapper">
            <img src="assets/images/jobs/char_acc_44.png" class="job-img" alt="">
            <p class="job-name" data-name="Accretia">Accretia</p>
          </div>
        </a>

      </div>


      <div id="bellatoInfo" class="jobs-details-info hide">
        <div class="go-back">
          <a href="#"><span class="chevron"></span> Go Back</a> 
        </div>
        <div class="job-wrapper">
          <img src="assets/images/jobs/char_bel_m_46.png" class="job-img" alt="">
          <p class="job-name">Bellato</p>
          <p class="rotate-job-name" >Bellato</p>
        </div>
        <div class="job-info">
          <div class="top-content">
            <p class="job-name">Union<strong>Bellato</strong></p>
            <p class="desc">Bellatians are a highly intelligent and innovative nation. The home planet of the Union has strong gravity which is why Bellatians are of short stature. While roaming space in search of resources Union forces discovered Novus and began colonization focusing on mining.</p>
          </div>
          <div class="bottom-content">
            <div class="class-detail box-border">
              <div class="box-border-top"></div>
              <div class="box-border-bottom"></div>
              <p class="box-title">Classes in Details</p>
              <div class="classes-wrapper">
                <div class="classes col">
                  <div class="img-wrapper">
                    <img src="assets/images/classes/bellato/warrior.png" alt="">
                  </div>
                  <p class="classes-name">Warrior</p>
                </div>
                <div class="classes col">
                  <div class="img-wrapper">
                    <img src="assets/images/classes/bellato/ranger.png" alt="">
                  </div>
                  <p class="classes-name">Ranger</p>
                </div>
                <div class="classes col">
                  <div class="img-wrapper">
                    <img src="assets/images/classes/bellato/specialist.png" alt="">
                  </div>
                  <p class="classes-name">specialist</p>
                </div>
                <div class="classes col">
                  <div class="img-wrapper">
                    <img src="assets/images/classes/bellato/caster.png" alt="">
                  </div>
                  <p class="classes-name">caster</p>
                </div>
              </div>
            </div>
            <div class="population box-border">
              <div class="box-border-top"></div>
              <div class="box-border-bottom"></div>
              <p class="box-title">Population</p>
              <img src="assets/images/classes/bellato/bellato-logo.png" alt="">
              <p class="pop-count">348</p>
            </div>
          </div>
        </div>
      </div>

    </div>

    <img src="assets/images/bg/svg1.svg" alt="" id="svg1">
    <img src="assets/images/bg/svg2.svg" alt="" id="svg2">
    <img src="assets/images/bg/svg3.svg" alt="" id="svg3">
    <img src="assets/images/bg/svg4.svg" alt="" id="svg4">
  </section>

<?php @include "footer.php"; ?>