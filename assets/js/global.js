$(document).ready(function(){

  new WOW().init();

//   $(document).scroll(function () {
//     var $nav = $(".fixed-top");
//     $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
//   });

	$('.jobs-section .job-container').on('click', function(e){
		e.preventDefault();
		var div_id = $(this).attr('href');
		$(div_id).toggleClass('hide');
	});

	$('.go-back').on('click',function(e){
		e.preventDefault();
		$(this).parent().toggleClass('hide');
	});

});