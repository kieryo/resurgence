<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>RF - Resurgence</title>

    <link rel="stylesheet" href="assets/libraries/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/libraries/animate.css"/>
    <link rel="stylesheet" href="assets/css/style.css">
  </head>

  <body>
    
    <?php if (strpos($_SERVER['SCRIPT_NAME'], 'index.php') !== false) : ?>
      <section class="main-banner">
        <nav class="navbar navbar-homepage">
          <ul class="navbar-nav d-flex w-100">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="rules-and-rates.php">Rules & Rates</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php">About Resurgence</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="downloads.php">Donate</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="downloads.php">Downloads</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact Us</a>
            </li>
        </nav>
        <div class="banner-content">
          <img src="assets/images/logo.png" class="wow pulse" alt="Resurgence Logo">
          <div class="login-register-link">
            <a href="#" class="login wow fadeIn" data-wow-delay="0.5s">Login <span class="circle"></span></a>
            <a href="#" class="login wow fadeIn" data-wow-delay="0.5s">Register <span class="circle"></span></a>
          </div>
        </div>
        <div class="overlay"></div>
      </section>
    <?php endif; ?>

    <nav class="main-navbar navbar navbar-expand-md">
      
      <h1 class="navbar-brand"><a href="#"><img src="assets/images/logo.png" alt="Resurgence Logo"> RF Resurgence</a></h1>
      
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="mainNavbar">
        <ul class="navbar-nav d-flex w-100">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rules-and-rates.php">Rules & Rates</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">About Resurgence</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Donate</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="downloads.php">Downloads</a>
          </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact Us</a>
            </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>