
<?php @include "header.php"; ?>

<div class="inside-banner">
  <img src="assets/images/inside-banner.jpg" alt="Inside Banner">
  <div class="banner-content">
    <h2 class="banner-title">About Resurgence</h2>
  </div>
  <div class="shadow"></div>
</div>

<main role="main" class="inside-pages main-wrapper about-resurgence">

  <div class="about-container">
    <p class="resurgence-info">RF Online is set in a distant planet in the Novus system where magic exists alongside high technology. Like most MMORPGs it follows the typical fantasy setting complete with swords and sorcery, but it also emphasizes the three-way Race vs. Race vs. Race (RvRvR) concept and modern/futuristic technology such as mecha and nuclear weapons.</p>
  </div>

  <div class="about-main-wrapper">

    <div class="about-wrapper bellato wow fadeIn">
      <img src="assets/images/about/bellato-logo.png" alt="" class="job-logo">
      <img src="assets/images/bg/about-bellatio-svg.svg" alt="" class="svg" id="bellatioSvg">
      <div class="about-container">
        <img src="assets/images/jobs/char_bel_m_46.png" alt="" class="job-img">
        <div class="desc">
          <p>As with many MMORPGs, the player will select one race and fight monsters to gain experience points. RFO is populated with many different monsters across many areas. At the start of the game, a character will be led through an in-game guide of the basic commands for the game. From there, the character will be exposed to a series of quests which will familiarize him/her with their character's headquarters. As the character progresses in level, his/her quests may take them out to aggressive maps, where he/she will be exposed to the player killing Features of the game. One quest given in the Sette Desert on low levels makes players aware of constant presence and danger of enemy races.</p>
        </div>
      </div>
    </div>

    <div class="about-wrapper cora wow fadeIn">
      <img src="assets/images/about/cora-logo.png" alt="" class="job-logo">
      <img src="assets/images/bg/about-cora-svg.svg" alt="" class="svg" id="coraSvg">
      <div class="about-container">
        <div class="desc">
          <p>There are 3 character races which the player can choose: <span class="red">Bellato Union, Holy Alliance Cora, and Accretian Empire</span> each having a unique race specialty. Every race has their own special sub-classes of characters to play based on warrior, ranger, specialist and each race's special classes. The Accretian Empire relies heavily on technology, so they lack access to magic. The Holy Alliance of Cora uses both magic and technology, but relies more on offensive magic. The Bellato Union uses both magic and technology but relies more on technology for offense and uses magic more on support. Class advancements become available at level 30 and 40. It is also possible to cross-class a character within the races for a wide variety of options.</p>
        </div>
        <img src="assets/images/jobs/char_cor_m_45.png" alt="" class="job-img">
      </div>
    </div>

    <div class="about-wrapper accretia wow fadeIn">
      <img src="assets/images/about/accretia-logo.png" alt="" class="job-logo">
      <img src="assets/images/bg/about-accretia-svg.svg" alt="" class="svg" id="accretiaSvg">
      <div class="about-container">
        <img src="assets/images/jobs/char_acc_44.png" alt="" class="job-img">
        <div class="desc">
          <p>Unlike most MMORPGs, RF Online revolves around PvP (Player vs Player). Only the racial headquarters, and select portal locations, are considered as safe areas from enemy races. Player killing comes into play because of the battle for resources between the three races. By default, opposing races members may engage in combat at any point in time on aggressive maps. Same race players may not engage in combat, unless by virtue of an item called the Chaos Potion, or during a special in-race competition called the Guild Circle Zone Scramble, also known as Guild Wars.</p>
        </div>
      </div>
    </div>

  </div>


<?php @include "footer.php"; ?>