

      <section class="register-now-section">
        <img src="assets/images/register-now.jpg" class="register-now-bg" alt="">
        <div class="banner-content">
          <h3 class="banner-title">REGISTER NOW TO PLAY</h3>
          <div class="login-register-link">
            <a href="#" class="login wow fadeIn" data-wow-delay="0.3s">Login <span class="circle"></span></a>
            <a href="#" class="login wow fadeIn" data-wow-delay="0.3s">Register <span class="circle"></span></a>
          </div>
        </div>
      </section>

    </main><!-- /.container -->

    <footer class="site-footer">
      <img src="assets/images/logo.png" class="footer-logo" alt="Resurgence Logo">
      <a href="#" class="fb-like">Like us at <img src="assets/images/fb.png" alt="FB Icon"></a>
      <p class="copyright">&copy;Copyright 2020 Rising Force Resurgence | All Rights Reserved</p>
      <img src="assets/images/bg/footer-svg1.svg" alt="" class="svg" id="footer-svg1">
      <img src="assets/images/bg/footer-svg2.svg" alt="" class="svg" id="footer-svg2">
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/libraries/jquery-3.5.1.min.js"></script>
    <script src="assets/libraries/popper.min.js"></script>
    <script src="assets/libraries/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/libraries/wow/wow.min.js"></script>
    <script src="assets/js/global.js"></script>
  </body>
</html>
