
<?php @include "header.php"; ?>

<div class="inside-banner">
  <img src="assets/images/inside-banner.jpg" alt="Inside Banner">
  <div class="banner-content">
    <h2 class="banner-title">Rules</h2>
    <p class="subtitle">PLEASE READ CAREFULLY</p>
  </div>
  <div class="shadow"></div>
</div>

<main role="main" class="inside-pages main-wrapper rules-and-rates">

  <div class="rules-rates-container">

    <div class="categories">
      <div class="box-border">
        <div class="box-border-top"></div>
        <div class="box-border-bottom"></div>
        <p class="title">Categories</p>
        <ul>
          <li><a href="#permanentBan">Permanent Ban</a></li>
          <li><a href="#gmsDecision">GM's Decision</a></li>
          <li><a href="#otherBanning">Other Banning Rules</a></li>
        </ul>
      </div>
    </div>

    <div class="rules-container">
      
      <div id="permanentBan" class="rules-wrapper permanent-ban">
        <div class="icon"></div>
        <div class="rules-list">
          <p class="rule-name">Permanent Ban</p>
          <ul>
            <li>IT IS FORBIDDEN TO PROMOTE OTHER SERVERS.</li>
            <li>DO NOT USE ANY 3RD PARTY PROGRAMS TO CHEAT.</li>
          </ul>
        </div>
      </div>
      
      <div id="gmsDecision" class="rules-wrapper gms-decision">
        <div class="icon"></div>
        <div class="rules-list">
          <p class="rule-name">GM's Decision. 3 days, Permanent ban next offense.</p>
          <ul>
            <li>USING CHAOS POTS INTENTIONALLY DURING CW</li>
            <li>INSULTS AND THREATS TO GM</li>
            <li>DEBUFFING ON SAFEZONE IS NOT ALLOWED.</li>
            <li>HEALING OTHER CHARACTER ON SAFEZONE IS NOT ALLOWED.</li>
            <li>PARKING MAU INSIDE THE CHIP IS NOT ALLOWED.</li>
            <li>BLOCKING NPCS IS NOT ALLOWED.</li>
            <li>CB BUFF BUG IS NOT ALLOWED. TO REMOVE IT YOU NEED TO JOIN TO GUILD AGAIN.</li>
            <li>FAKE NEWS OF ANY KIND REGARDING THE SERVER IS NOT ALLOWED.</li>
            <li>CAUSING RACE ISSUES AND OFFENSIVE PLAYERS ARE NOT ALLOWED IN THIS SERVER.</li>
            <li>RMT INSIDE THE "GAME" IS NOT ALLOWED.</li>
          </ul>
        </div>
      </div>
      
      <div id="otherBanning" class="rules-wrapper other-banning">
        <div class="icon"></div>
        <div class="rules-list">
          <p class="rule-name">Other Banning Rules</p>
          <ul>
            <li>DO NOT USE ANY KIND OF BUGS <p>7 days ban, 2nd offense Permanent Ban</p></li>
            <li>CUT SKILLS ARE NOT ALLOWED <p>1st Offense 3 days, Permanent ban next offense.</p></li>
            <li>ABUSING CHAOS POTION IN SAME RACE IN ANY MAP IS NOT ALLOWED. <p>1st Offense 3 days, Permanent ban next offense.</p></li>
            <li>FORCE LOG OUT IS NOT ALLOWED. <p>3 days, Permanent ban next offense.</p></li>
            <li>POINTS FEEDING IS NOT ALLOWED. <p>3 days, Permanent ban next offense.</p></li>
          </ul>
        </div>
      </div>

    </div>
  </div>

  <div class="rates-container">
    <div class="container-title">
      <h3 class="title">Rates</h3>
    </div>
    <div class="container-content">
      <div class="row">
        <div class="rate-wrapper col-md-2">
          <p class="title">Player</p>
          <p class="rate">EXP X2</p>
          <p class="premium">Premium x4</p>
        </div>
        <div class="rate-wrapper col-md-2">
          <p class="title">Animus</p>
          <p class="rate">X32</p>
        </div>
        <div class="rate-wrapper col-md-2">
          <p class="title">Drop</p>
          <p class="rate">X2</p>
        </div>
        <div class="rate-wrapper col-md-2">
          <p class="title">Mine</p>
          <p class="rate">X4</p>
        </div>
      </div>
    </div>

    <img src="assets/images/bg/rules-rates-svg1.svg" alt="" class="svg" id="rulesRatesSvg1">
    <img src="assets/images/bg/rules-rates-svg2.svg" alt="" class="svg" id="rulesRatesSvg2">
  </div>

<?php @include "footer.php"; ?>