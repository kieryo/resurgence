
<?php @include "header.php"; ?>

<div class="inside-banner">
  <img src="assets/images/inside-banner.jpg" alt="Inside Banner">
  <div class="banner-content">
    <h2 class="banner-title">Contact Us</h2>
    <p class="subtitle">GET IN TOUCH WITH US!</p>
  </div>
  <div class="shadow"></div>
</div>

<main role="main" class="inside-pages main-wrapper contact-us">

  <div class="contact-us-wrapper">
    
      <p>Do you have any concerns about the game? Don't hesitate to message us!</p>

      <form action="">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Name">
        </div>
        <div class="form-group">
          <input type="email" class="form-control" placeholder="Email Address">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Phone">
        </div>
        <div class="form-group">
          <textarea name="" id="" cols="30" rows="5" class="form-control" placeholder="Message"></textarea>
        </div>
        <button type="submit" name="btn">Submit <span class="chevron"></span></button>
      </form>

  </div>

<?php @include "footer.php"; ?>